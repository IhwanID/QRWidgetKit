//
//  QrWidget.swift
//  QrWidget
//
//  Created by Ihwan on 30/04/21.
//

import WidgetKit
import SwiftUI
import CoreImage.CIFilterBuiltins

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(qrcode:  "hello")
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(qrcode: Provider.getStringQrCode())
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(qrcode: Provider.getStringQrCode())
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
    
    private static func getStringQrCode() -> String{
        return DataProvider.shared.getStringQrCodeFromAnyWhere()
    }
}

struct SimpleEntry: TimelineEntry {
    var date: Date {
        return Date()
    }
    let qrcode: String
}

struct QrWidgetEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        //Text(entry.date, style: .time)
        VStack{
            Text("Your QRCode")
            Image(uiImage: generateQRCode(from: entry.qrcode))
                .resizable()
                .scaledToFit()
                
        }
    }
    
    func generateQRCode(from string: String) -> UIImage {
        let context = CIContext()
        let filter = CIFilter.qrCodeGenerator()
        let data = Data(string.utf8)
        filter.setValue(data, forKey: "inputMessage")

        if let outputImage = filter.outputImage {
            if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
                return UIImage(cgImage: cgimg)
            }
        }

        return UIImage(systemName: "xmark.circle") ?? UIImage()
    }
}

@main
struct QrWidget: Widget {
    let kind: String = "QrWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            QrWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct QrWidget_Previews: PreviewProvider {
    static var previews: some View {
        QrWidgetEntryView(entry: SimpleEntry(qrcode: "hello"))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
